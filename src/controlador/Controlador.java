package controlador;

import vista.dlgIngreso;
import vista.dlgLista;
import vista.dlgRegistro;
import modelo.Usuarios;
import modelo.dbUsuarios;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener{
    private dlgIngreso ingreso;
    private dlgLista lista;
    private dlgRegistro registro;
    private Usuarios users;
    private dbUsuarios db;
    private boolean A = false;
    
    public Controlador(Usuarios users, dbUsuarios db, dlgIngreso ingreso, dlgLista lista, dlgRegistro registro){
        this.users = users;
        this.db = db;
        this.ingreso = ingreso;
        this.lista = lista;
        this.registro = registro;
        
        ingreso.btnIngresar.addActionListener(this);
        ingreso.btnRegistrar.addActionListener(this);
        lista.btnCerrarL.addActionListener(this);
        registro.btnGuardarR.addActionListener(this);
        registro.btnLimpiarR.addActionListener(this);
        registro.btnCerrarR.addActionListener(this);
    }
    
    public void iniciarIngreso(){
        ingreso.setTitle(":: USUARIOS ::");
        ingreso.setSize(450, 350);
        ingreso.setLocationRelativeTo(null);
        ingreso.setVisible(true);
    }
    
    public void iniciarLista(){
        lista.setTitle(":: USUARIOS ::");
        lista.setSize(450, 350);
        lista.setLocationRelativeTo(null);
        lista.setVisible(true);
    }
    
    public void iniciarRegistro(){
        registro.setTitle(":: USUARIOS ::");
        registro.setSize(450, 350);
        registro.setLocationRelativeTo(null);
        registro.setVisible(true);
    }
    
    private void closeI(){
        ingreso.dispose();
    }
    
    private void closeL(){
        lista.dispose();
    }
    
    private void closeR(){
        registro.dispose();
    }
    
    public void limpiar(){
        registro.txtCContraseñaR.setText("");
        registro.txtContraseñaR.setText("");
        registro.txtCorreoR.setText("");
        registro.txtUsuarioR.setText("");
        A = (false);
    }
    
    public void cargarDatos(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Usuarios> listas = new ArrayList<>();
        try{
            listas = db.listar();
        }
        catch(Exception ex){
            
        }
        modelo.addColumn("id");
        modelo.addColumn("Usuario");
        modelo.addColumn("Correo");
        for(Usuarios producto : listas){
            modelo.addRow(new Object[]{producto.getIdUsuario(), producto.getUsuario(), 
                producto.getCorreo(), producto.getContraseña()});
        }
        lista.tblLista.setModel(modelo);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == ingreso.btnIngresar){
            try{
                Usuarios users = new Usuarios();
                if(db.isExiste(ingreso.txtUsuario.getText())){
                    users = (Usuarios) db.buscar(ingreso.txtUsuario.getText());
                    if(ingreso.txtContraseña.getText().equals(users.getContraseña())){
                        JOptionPane.showMessageDialog(null, "Sucessfull Login");
                        closeI();
                        iniciarLista();
                        db.listar();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Password or User Incorrect");
                        return;
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "Usuario no registrado");
                    return;
                }
            }
            catch(SQLException d){
                d.printStackTrace();
            }
            catch(Exception ex1){
                System.err.println("Surgio un error" + ex1);
            }
        }
        if(e.getSource() == ingreso.btnRegistrar){
            int option = JOptionPane.showConfirmDialog(ingreso, "¿Desea crear un nuevo usuario?", 
                    "Decide", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_NO_OPTION){
                ingreso.dispose();
                closeI();
                iniciarRegistro();
            }
        }
        if(e.getSource() == registro.btnGuardarR){
            try{
                if("".equalsIgnoreCase(registro.txtUsuarioR.getText())){
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe un usuario");
                }
                if(db.isExiste(registro.txtUsuarioR.getText())){
                    JOptionPane.showMessageDialog(registro, "El usuasrio ya existe");
                    return;
                }
                if("".equalsIgnoreCase(registro.txtCorreoR.getText())){
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe un correo");
                }
                if("".equalsIgnoreCase(registro.txtCContraseñaR.getText())){
                    throw new IllegalArgumentException("Campo vacio. \nEscribe una contraseña");
                }
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(registro, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }
            catch (Exception ex2) {
                JOptionPane.showMessageDialog(registro, "Error: " + ex2.getMessage());
                return;
            }
            try{
                users.setUsuario(registro.txtUsuarioR.getText());
                users.setCorreo(registro.txtCorreoR.getText());
                users.setContraseña(registro.txtContraseñaR.getText());
                if(registro.txtContraseñaR.getText().equals(registro.txtCContraseñaR.getText()))
                {
                    users.setCtr2(registro.txtCContraseñaR.getText());
                }
                else{
                    JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden");
                    return;
                }
                
                A = true;
                if (A) {
                    db.insertar(users);
                    JOptionPane.showMessageDialog(null, "Registro Guardado");
                    limpiar();
                    cargarDatos();
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        if(e.getSource() == registro.btnLimpiarR){
            limpiar();
        }
        
        if(e.getSource() == registro.btnCerrarR){
             int option = JOptionPane.showConfirmDialog(registro, "¿Regresar al Login?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                registro.dispose();
                closeR();
                iniciarIngreso();
            }
        }
        
        if(e.getSource() == lista.btnCerrarL){
                lista.dispose();
                iniciarIngreso();
        }
    }
    
    
    public static void main(String[] args) throws Exception{
        Usuarios users = new Usuarios();
        dbUsuarios db = new dbUsuarios();
        dlgIngreso ingreso = new dlgIngreso();
        dlgLista lista = new dlgLista();
        dlgRegistro registro = new dlgRegistro();
        Controlador contra = new Controlador(users, db, ingreso, lista, registro);
        contra.cargarDatos();
        contra.iniciarIngreso();
    }

}
