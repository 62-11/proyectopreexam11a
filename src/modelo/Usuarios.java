/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author luism,enrique,oscar
 */
public class Usuarios {
    
    private int idUsuario;
    private String usuario;
    private String correo;
    private String contraseña;
    private String contraseña2;

    public Usuarios(int idUsuario, String usuario, String correo, String contraseña, String contraseña2) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.correo = correo;
        this.contraseña = contraseña;
        this.contraseña2 = contraseña2;
    }
    
    public Usuarios(){
        
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getContraseña2() {
        return contraseña2;
    }

    public void setCtr2(String contra2) {
        this.contraseña2 = contra2;
    }
    
    
}
