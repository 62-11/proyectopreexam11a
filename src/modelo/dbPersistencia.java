/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author luism,enrique,oscar
 */
public interface dbPersistencia {
    
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    public void borrar(Object objeto, String codigo) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    
    public Object buscar(String codigo) throws Exception;
}
