/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author luism,enrique,oscar
 */
public class dbUsuarios extends dbManejador implements dbPersistencia{
    @Override
    public void insertar(Object objeto) throws Exception {
        Usuarios user = new Usuarios();
        user = (Usuarios) objeto;
        String consulta = "insert into " + "usuarios(nombre, correo, contraseña) values (?, ?, ?)";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                this.sqlConsulta.setString(1, user.getUsuario());
                this.sqlConsulta.setString(2, user.getCorreo());
                this.sqlConsulta.setString(3, user.getContraseña());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Usuarios user = new Usuarios();
        user = (Usuarios) objeto;
        String consulta = "update usuarios set correo = ?, contraseña = ?, where nombre = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta

                this.sqlConsulta.setString(1, user.getCorreo());
                this.sqlConsulta.setString(2, user.getContraseña());
                this.sqlConsulta.setString(3, user.getUsuario());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void borrar(Object objeto, String codigo) throws Exception {
        Usuarios user = new Usuarios();
        user = (Usuarios) objeto;
        String consulta = "DELETE from usuarios WHERE nombre = ?";
                
        if(conectar()){
            try{
                System.err.println("Se conecto");
                 this.sqlConsulta = this.conexion.prepareStatement(consulta);
                

                this.sqlConsulta.setString(1, codigo);
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.out.println("Error al borrar " + e.getMessage());
            }
        }else{
            System.out.println("No fue posible conectarse");
        }

    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean confirm = false;
        if(this.conectar()){
            String consulta = "Select * from usuarios where nombre = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                confirm = true;
            }
        }
        this.desconectar();
        return confirm;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Usuarios> lista = new ArrayList<Usuarios>();
        Usuarios user;
        if(this.conectar()){
            String consulta = "Select * from usuarios order by nombre";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                user = new Usuarios();
                user.setIdUsuario(this.registros.getInt("idusuarios"));
                user.setUsuario(this.registros.getString("nombre"));
                user.setCorreo(this.registros.getString("correo"));
                lista.add(user);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Usuarios user = new Usuarios();
        if(conectar()){
            String consulta = "SELECT * from usuarios WHERE nombre = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                user.setUsuario(this.registros.getString("nombre"));
                user.setContraseña(this.registros.getString("contraseña"));
            }
        }
        this.desconectar();
        return user;
    }
 
}
